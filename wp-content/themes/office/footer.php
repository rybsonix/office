<?php
/**
 * The template for displaying the footer.
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
 ?>

				<footer class="footer" role="contentinfo">

					<div class="inner-footer grid-x grid-margin-x grid-padding-x">

            <div class="scroll-button">
        				<i class="fas fa-chevron-up"></i>
        		</div>

					</div> <!-- end #inner-footer -->

				</footer> <!-- end .footer -->

      </div>  <!-- end .off-canvas-content -->

		</div> <!-- end .off-canvas-wrapper -->

		<?php wp_footer(); ?>

	</body>

</html> <!-- end page -->
