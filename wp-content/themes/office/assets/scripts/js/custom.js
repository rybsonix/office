(function( root, $, undefined ) {

    $(function () {

        $(".gt_switcher .gt_selected a, .gt_switcher .gt_option a").contents().filter(function(){
            return (this.nodeType == 3);
        }).remove();

        var scrollButton = $('.scroll-button');
        $(window).scroll(function(e){
            e.preventDefault();
            if ($(this).scrollTop() > 600) {;
                scrollButton.fadeIn()
            } else {
                scrollButton.fadeOut();
            };
        });
        scrollButton.click(function(){
            $("html, body").animate({ scrollTop: 0 }, 500);
        });

        var swiperH = new Swiper('.swiper-container-h', {
            loop: true,
            autoplay:{
                delay: 5000,
            },
            keyboard: {
                enabled: true
            },
            spaceBetween: 0,
            pagination: {
                el: '.swiper-pagination-h',
                clickable: true,
            },
        });

        $('#main-nav .menu-item a, #offcanvas-nav .menu-item a').click(function(){
            if ($(this).parent().index() == 0) {
                $('html, body').animate({
                    scrollTop: $("#about").offset().top
                }, 700);
            }
            if ($(this).parent().index() == 1) {
                $('html, body').animate({
                    scrollTop: $("#team").offset().top
                }, 700);
            }
            if ($(this).parent().index() == 2) {
                $('html, body').animate({
                    scrollTop: $("#offer").offset().top
                }, 700);
            }
            if ($(this).parent().index() == 3) {
                $('html, body').animate({
                    scrollTop: $("#contact").offset().top
                }, 700);
            }
        });

        $('body:not(.home) #main-nav .menu-item a, body:not(.home) #offcanvas-nav .menu-item a').each(function(){
            if ($(this).parent().index() == 0) $(this).attr('href', '/#about');
            if ($(this).parent().index() == 1) $(this).attr('href', '/#team');
            if ($(this).parent().index() == 2) $(this).attr('href', '/#offer');
            if ($(this).parent().index() == 3) $(this).attr('href', '/#contact');
        });

        $('#team .button').click(function(e){
            e.preventDefault();
            $(this).parents('.card').addClass('active');
        });

        $('#team .close').click(function(e){
            e.preventDefault();
            $(this).parents('.card').removeClass('active');
        });

        $('#offcanvas-nav .menu-item a').attr('data-close', '');

        var scrollIcon = function(){
            $('.content-hidden').each(function(){
                if($('.hidden-container').height() < $(this).height()) $(this).parent().parent().next().addClass('active');
            });
        };

        scrollIcon();

        $( window ).resize(function() {
            scrollIcon();
        });

    });

} ( this, jQuery ));
