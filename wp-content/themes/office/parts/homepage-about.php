<?php ;?>

<section id="about" class="slide-section" style="background-image: url(<?php the_field('about_bg'); ?>); background-size: cover;">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
              <div class="cell">
                  <h2>O kancelarii</h2>
              </div>
              <div class="small-12 large-9 cell">
                  <?php the_field('about_content'); ?>
              </div>
        </div>
    </div>
</section>

<?php ;?>
