<?php ;?>

<section id="offer" class="slide-section">
    <div class="grid-container">
        <h2 class="section-title">Oferta</h2>
        <div class="grid-x grid-margin-x grid-margin-y" data-equalizer>
              <div class="small-12 medium-6 large-4 xlarge-3 cell">
                  <div class="card" data-equalizer-watch>
                      <?php the_field('offer1'); ?>
                      <div>
                          <img src="<?php the_field('offer1_icon'); ?>" alt="Opinie ikona">
                          <p class="button-container"><button class="button hollow" data-open="card-reveal1">więcej</button></p>
                      </div>
                  </div>
                  <div class="reveal large" id="card-reveal1" data-reveal>
                    <div class="reveal-container">
                      <div class="grid-x grid-margin-x">
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--content">
                                  <div class="reveal-container--content-icon">
                                      <img src="<?php the_field('offer1_icon'); ?>" alt="Opinie ikona">
                                  </div>
                                  <h3><?php the_field('offer1'); ?></h3>
                                  <?php the_field('offer1_reveal_text'); ?>
                              </div>
                          </div>
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--img">
                                  <img src="<?php the_field('offer1_reveal_img'); ?>">
                              </div>
                          </div>
                      </div>
                    </div>
                      <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              </div>
              <div class="small-12 medium-6 large-4 xlarge-3 cell">
                  <div class="card" data-equalizer-watch>
                      <?php the_field('offer2'); ?>
                      <div>
                          <img src="<?php the_field('offer2_icon'); ?>" alt="Obsługa prawna przedsiębiorców">
                          <p class="button-container"><button class="button hollow" data-open="card-reveal2">więcej</button></p>
                      </div>
                  </div>
                  <div class="reveal large" id="card-reveal2" data-reveal>
                    <div class="reveal-container">
                      <div class="grid-x grid-margin-x">
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--content">
                                  <div class="reveal-container--content-icon">
                                      <img src="<?php the_field('offer2_icon'); ?>" alt="Opinie ikona">
                                  </div>
                                  <h3><?php the_field('offer2'); ?></h3>
                                  <?php the_field('offer2_reveal_text'); ?>
                              </div>
                          </div>
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--img">
                                  <img src="<?php the_field('offer2_reveal_img'); ?>">
                              </div>
                          </div>
                      </div>
                    </div>
                      <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              </div>
              <div class="small-12 medium-6 large-4 xlarge-3 cell">
                  <div class="card" data-equalizer-watch>
                      <?php the_field('offer3'); ?>
                      <div>
                          <img src="<?php the_field('offer3_icon'); ?>" alt="Szkolenia i warsztaty">
                          <p class="button-container"><button class="button hollow" data-open="card-reveal3">więcej</button></p>
                      </div>
                  </div>
                  <div class="reveal large" id="card-reveal3" data-reveal>
                    <div class="reveal-container">
                      <div class="grid-x grid-margin-x">
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--content">
                                  <div class="reveal-container--content-icon">
                                      <img src="<?php the_field('offer3_icon'); ?>" alt="Opinie ikona">
                                  </div>
                                  <h3><?php the_field('offer3'); ?></h3>
                                  <?php the_field('offer3_reveal_text'); ?>
                              </div>
                          </div>
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--img">
                                  <img src="<?php the_field('offer3_reveal_img'); ?>">
                              </div>
                          </div>
                      </div>
                    </div>
                      <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              </div>
              <div class="small-12 medium-6 large-4 xlarge-3 cell">
                  <div class="card" data-equalizer-watch>
                      <?php the_field('offer4'); ?>
                      <div>
                          <img src="<?php the_field('offer4_icon'); ?>" alt="Sprawy karne">
                          <p class="button-container"><button class="button hollow" data-open="card-reveal4">więcej</button></p>
                      </div>
                  </div>
                  <div class="reveal large" id="card-reveal4" data-reveal>
                    <div class="reveal-container">
                      <div class="grid-x grid-margin-x">
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--content">
                                  <div class="reveal-container--content-icon">
                                      <img src="<?php the_field('offer4_icon'); ?>" alt="Opinie ikona">
                                  </div>
                                  <h3><?php the_field('offer4'); ?></h3>
                                  <?php the_field('offer4_reveal_text'); ?>
                              </div>
                          </div>
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--img">
                                  <img src="<?php the_field('offer4_reveal_img'); ?>">
                              </div>
                          </div>
                      </div>
                    </div>
                      <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              </div>
              <div class="small-12 medium-6 large-4 xlarge-3 cell">
                  <div class="card" data-equalizer-watch>
                      <?php the_field('offer5'); ?>
                      <div>
                          <img src="<?php the_field('offer5_icon'); ?>" alt="Porady prawne">
                          <p class="button-container"><button class="button hollow" data-open="card-reveal5">więcej</button></p>
                      </div>
                  </div>
                  <div class="reveal large" id="card-reveal5" data-reveal>
                    <div class="reveal-container">
                      <div class="grid-x grid-margin-x">
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--content">
                                  <div class="reveal-container--content-icon">
                                      <img src="<?php the_field('offer5_icon'); ?>" alt="Opinie ikona">
                                  </div>
                                  <h3><?php the_field('offer5'); ?></h3>
                                  <?php the_field('offer5_reveal_text'); ?>
                              </div>
                          </div>
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--img">
                                  <img src="<?php the_field('offer5_reveal_img'); ?>">
                              </div>
                          </div>
                      </div>
                    </div>
                      <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              </div>
              <div class="small-12 medium-6 large-4 xlarge-3 cell">
                  <div class="card" data-equalizer-watch>
                      <?php the_field('offer6'); ?>
                      <div>
                          <img src="<?php the_field('offer6_icon'); ?>" alt="Sprawy administracyjne">
                          <p class="button-container"><button class="button hollow" data-open="card-reveal6">więcej</button></p>
                      </div>
                  </div>
                  <div class="reveal large" id="card-reveal6" data-reveal>
                    <div class="reveal-container">
                      <div class="grid-x grid-margin-x">
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--content">
                                  <div class="reveal-container--content-icon">
                                      <img src="<?php the_field('offer6_icon'); ?>" alt="Opinie ikona">
                                  </div>
                                  <h3><?php the_field('offer6'); ?></h3>
                                  <?php the_field('offer6_reveal_text'); ?>
                              </div>
                          </div>
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--img">
                                  <img src="<?php the_field('offer6_reveal_img'); ?>">
                              </div>
                          </div>
                      </div>
                    </div>
                      <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              </div>
              <div class="small-12 medium-6 large-4 xlarge-3 cell">
                  <div class="card" data-equalizer-watch>
                      <?php the_field('offer7'); ?>
                      <div>
                          <img src="<?php the_field('offer7_icon'); ?>" alt="Sprawy cywilne">
                          <p class="button-container"><button class="button hollow" data-open="card-reveal7">więcej</button></p>
                      </div>
                  </div>
                  <div class="reveal large" id="card-reveal7" data-reveal>
                    <div class="reveal-container">
                      <div class="grid-x grid-margin-x">
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--content">
                                  <div class="reveal-container--content-icon">
                                      <img src="<?php the_field('offer7_icon'); ?>" alt="Opinie ikona">
                                  </div>
                                  <h3><?php the_field('offer7'); ?></h3>
                                  <?php the_field('offer7_reveal_text'); ?>
                              </div>
                          </div>
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--img">
                                  <img src="<?php the_field('offer7_reveal_img'); ?>">
                              </div>
                          </div>
                      </div>
                    </div>
                      <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              </div>
              <div class="small-12 medium-6 large-4 xlarge-3 cell">
                  <div class="card" data-equalizer-watch>
                      <?php the_field('offer8'); ?>
                      <div>
                          <img src="<?php the_field('offer8_icon'); ?>" alt="Przygotowywanie i opiniowanie projektów umów">
                          <p class="button-container"><button class="button hollow" data-open="card-reveal8">więcej</button></p>
                      </div>
                  </div>
                  <div class="reveal large" id="card-reveal8" data-reveal>
                    <div class="reveal-container">
                      <div class="grid-x grid-margin-x">
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--content">
                                  <div class="reveal-container--content-icon">
                                      <img src="<?php the_field('offer8_icon'); ?>" alt="Opinie ikona">
                                  </div>
                                  <h3><?php the_field('offer8'); ?></h3>
                                  <?php the_field('offer8_reveal_text'); ?>
                              </div>
                          </div>
                          <div class="cell medium-12 large-6">
                              <div class="reveal-container--img">
                                  <img src="<?php the_field('offer8_reveal_img'); ?>">
                              </div>
                          </div>
                      </div>
                    </div>
                      <button class="close-button" data-close aria-label="Close modal" type="button">
                        <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
              </div>
        </div>
    </div>
</section>

<?php ;?>
