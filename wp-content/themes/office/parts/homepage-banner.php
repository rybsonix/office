<section id="banner" class="slide-section">
    <div class="swiper-container swiper-container-h">
        <div class="swiper-wrapper">
            <div class="swiper-slide" style="background-image: url(<?php the_field('banner_slide1'); ?>)">
                <div class="banner-text">
                    <?php the_field('banner_slide1_text'); ?>
                </div>
            </div>
            <div class="swiper-slide" style="background-image: url(<?php the_field('banner_slide2'); ?>)">
                <div class="banner-text">
                    <?php the_field('banner_slide2_text'); ?>
                </div>
            </div>
            <div class="swiper-slide" style="background-image: url(<?php the_field('banner_slide3'); ?>)">
                <div class="banner-text">
                    <?php the_field('banner_slide3_text'); ?>
                </div>
            </div>
        </div>
        <div class="swiper-pagination swiper-pagination-h"></div>
    </div>
</section>
