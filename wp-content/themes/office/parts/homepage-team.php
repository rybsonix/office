<?php ;?>

<section id="team" class="slide-section" style="background-image: url(<?php the_field("team_bg");?>)">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
              <div class="cell">
                  <h2>Zespół kancelarii</h2>
              </div>
              <div class="small-12 medium-6 large-4 cell">
                  <div class="card">
                      <img src="<?php the_field('team_member1'); ?>">
                      <div class="card__content">
                          <p><strong><?php the_field('team_member1_name'); ?></strong></p>
                          <?php the_field('team_member1_desc'); ?>
                          <button class="hollow button" href="#">+</button>
                          <button class="close"><span><strong>x</strong></span></button>
                          <div class="hidden-container">
                              <div class="content-hidden">
                                  <?php the_field('team_member1_hide'); ?>
                              </div>

                          </div>
                      </div>
                      <div class="scroll-button-icon">
                          <span class="mouse"></span>
                      </div>
                  </div>
              </div>
              <div class="small-12 medium-6 large-4 cell">
                  <div class="card">
                      <img src="<?php the_field('team_member2'); ?>">
                      <div class="card__content">
                          <p><strong><?php the_field('team_member2_name'); ?></strong></p>
                          <?php the_field('team_member2_desc'); ?>
                          <button class="hollow button" href="#">+</button>
                          <button class="close"><span><strong>x</strong></span></button>
                          <div class="hidden-container">
                              <div class="content-hidden">
                                  <?php the_field('team_member2_hide'); ?>
                              </div>
                          </div>
                      </div>
                      <div class="scroll-button-icon">
                          <span class="mouse"></span>
                      </div>
                  </div>
              </div>
              <div class="small-12 medium-6 large-4 cell">
                  <div class="card">
                      <img src="<?php the_field('team_member3'); ?>">
                      <div class="card__content">
                          <p><strong><?php the_field('team_member3_name'); ?></strong></p>
                          <?php the_field('team_member3_desc'); ?>
                          <button class="hollow button" href="#">+</button>
                          <button class="close"><span><strong>x</strong></span></button>
                          <div class="hidden-container">
                              <div class="content-hidden">
                                  <?php the_field('team_member3_hide'); ?>
                              </div>
                          </div>
                      </div>
                      <div class="scroll-button-icon">
                          <span class="mouse"></span>
                      </div>
                  </div>
              </div>
        </div>
    </div>
</section>

<?php ;?>
