<?php ;?>

<section id="contact" class="slide-section">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell">
                <h2>Kontakt</h2>
            </div>
              <div class="small-12 medium-6 cell">
                <div class="footer--map-container">
                    <?php the_field('contact_map'); ?>
                </div>
              </div>
              <div class="small-12 medium-6 cell">
                  <div class="contact-details">
                      <div class="contact-details-block">
                          <?php the_field('contact_text'); ?>
                          <div class="footer--logo-container">
                              <img src="<?php the_field('contact_logo'); ?>" alt="Footer Logo">
                          </div>
                      </div>
                  </div>
              </div>
        </div>
    </div>
</section>

<?php ;?>
